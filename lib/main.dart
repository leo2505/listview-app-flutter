import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

void main() => runApp(MaterialApp(
      home: Home(),
    ));

class Cliente {
  String nome;
  int idade;

  Cliente(this.nome, this.idade);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Cliente> clientes = List();

  TextEditingController nomeController = TextEditingController();
  TextEditingController idadeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    /*clientes.add(Cliente("Moacir", 35));
    clientes.add(Cliente("Maria", 42));
    clientes.add(Cliente("Joana", 22));
    clientes.add(Cliente("Clóvis", 15));
    clientes.add(Cliente("Airton", 17));*/

    for (Cliente cli in clientes) {
      print(cli.nome + " - " + cli.idade.toString());
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Teste List View"),
          centerTitle: true,
          backgroundColor: Colors.deepOrange,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.build), onPressed: () {}),
            PopupMenuButton<int>(
              itemBuilder: (context) => <PopupMenuEntry<int>>[
                PopupMenuItem<int>(
                  child: Text("Ordenar de A-Z"),
                  value: 1,
                ),
                PopupMenuItem<int>(
                  child: Text("Ordenar de Z-A"),
                  value: 2,
                ),
              ],
              onSelected: (int resultado) {
                setState(() {
                  if (resultado == 1) {
                    clientes.sort((a, b) {
                      return a.nome
                          .toLowerCase()
                          .compareTo(b.nome.toLowerCase());
                    });
                  } else{
                    clientes.sort((a, b) {
                      return b.nome
                          .toLowerCase()
                          .compareTo(a.nome.toLowerCase());
                    });
                  }
                });
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text("Cadastro de Clientes"),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      controller: nomeController,
                      decoration: InputDecoration(
                        labelText: "Nome",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: idadeController,
                      decoration: InputDecoration(
                        labelText: "Idade",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: RaisedButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      child: Text("Confirmar"),
                      onPressed: () {
                        var idadeInput = int.parse(idadeController.text);
                        setState(() {
                          bool jaTem = false;
                          for (Cliente cl in clientes) {
                            if (cl.nome == nomeController.text) jaTem = true;
                          }
                          if (jaTem == false) {
                            clientes
                                .add(Cliente(nomeController.text, idadeInput));
                          } else {
                            alerta();
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
              ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  padding: EdgeInsets.all(1),
                  itemCount: clientes.length,
                  itemBuilder: (BuildContext context, index) {
                    return montaItem(context, index);
                  }),
            ],
          ),
        ));
  }

  montaItem(BuildContext context, index) {
    return Card(
      child: ListTile(
          leading: Icon(Icons.person, size: 50, color: Colors.blue),
          title: Text(clientes[index].nome),
          subtitle: Text(clientes[index].idade.toString(),
              style: TextStyle(fontSize: 20)),
          trailing: IconButton(
            icon: Icon(Icons.delete),
            color: Colors.red,
            onPressed: () {
              alertaExcluir(index);
            },
          )),
    );
  }

  alerta() {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ops!"),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Center(child: Text("Já cadastrado")),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
  alertaExcluir(index) {
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Alerta!"),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Center(child: Text("Excluir cadastro?")),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Cancelar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              RaisedButton(
                textColor: Colors.white,
                child: Text("OK"),
                onPressed: () {
                  setState(() {
                  clientes.removeAt(index);
                  Navigator.of(context).pop();
                  });

                },
              )
            ],
          );
        });
  }
}
